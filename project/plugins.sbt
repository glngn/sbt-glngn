libraryDependencies += "org.scala-sbt" %% "scripted-plugin" % sbtVersion.value

addSbtPlugin("com.frugalmechanic" % "fm-sbt-s3-resolver" % "0.19.0")
addSbtPlugin("com.github.sbt"     % "sbt-dynver"         % "5.0.1")
addSbtPlugin("ch.epfl.scala"      % "sbt-scalafix"       % "0.11.1")
addSbtPlugin("org.scalameta"      % "sbt-scalafmt"       % "2.4.5")

// TODO: I have no idea why this magic is required. sbtix missing dep?
// libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.11.1" % "test" classifier "tests"
