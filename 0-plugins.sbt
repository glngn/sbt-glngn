addSbtPlugin("org.scalameta"  % "sbt-scalafmt" % "2.5.2")
addSbtPlugin("ch.epfl.scala"  % "sbt-scalafix" % "0.13.0")
addSbtPlugin("com.github.sbt" % "sbt-dynver"   % "5.0.1")

// https://github.com/sbt/sbt-assembly
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "1.1.0")

// https://github.com/tpunder/fm-sbt-s3-resolver
addSbtPlugin("com.frugalmechanic" % "fm-sbt-s3-resolver" % "0.20.0")

// https://github.com/spray/sbt-revolver
addSbtPlugin("io.spray" % "sbt-revolver" % "0.9.1")

// https://github.com/sbt/sbt-boilerplate
addSbtPlugin("io.spray" % "sbt-boilerplate" % "0.6.1")

// https://github.com/sbt/sbt-native-packager
addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.15")

// ???
addSbtPlugin("com.github.reibitto" % "sbt-welcome" % "0.2.1")

// https://github.com/portable-scala/sbt-crossproject
addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject" % "1.3.2")

addSbtPlugin("org.scala-js" % "sbt-scalajs" % "1.8.0")
