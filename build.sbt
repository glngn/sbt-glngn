import com.amazonaws.auth.{AWSCredentialsProviderChain, DefaultAWSCredentialsProviderChain}
import com.amazonaws.auth.profile.ProfileCredentialsProvider

name         := """sbt-glngn"""
organization := "glngn"

publishMavenStyle := false

publishTo := Some(
  Resolver.url(
    "glngn releases",
    url("s3://glngn-server-releases/releases")
  )(Resolver.ivyStylePatterns)
)

s3CredentialsProvider := { (bucket: String) =>
  val secretsFile = file("../../../../glngn-secrets/aws-credentials")
  if (secretsFile.exists) {
    new AWSCredentialsProviderChain(
      new ProfileCredentialsProvider(secretsFile.toString, "glngn-server-deploy"),
      DefaultAWSCredentialsProviderChain.getInstance()
    )
  } else {
    DefaultAWSCredentialsProviderChain.getInstance()
  }
}

sbtPlugin := true

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1" % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

// bintrayPackageLabels := Seq("sbt","plugin")
// bintrayVcsUrl := Some("""https://gitlab.com/glngn/sbt-glngn.git""")
// ThisBuild / bintrayReleaseOnPublish := false

console / initialCommands := """import glngn.sbt._"""

enablePlugins(ScriptedPlugin)

// set up 'scripted; sbt plugin for testing sbt plugins
scriptedLaunchOpts ++=
  Seq("-Xmx1024M", "-Dplugin.version=" + version.value)

scalacOptions ++= Seq(
  "-Ywarn-unused-import",
  "-Yrangepos"
)
