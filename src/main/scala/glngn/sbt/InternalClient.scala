package glngn.sbt

import org.portablescala.sbtplatformdeps.PlatformDepsPlugin.autoImport._
import org.scalajs.linker.interface.ESVersion
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt._
import sbt.Keys._
import sbtcrossproject.CrossPlugin.autoImport._
import sbtcrossproject.CrossProject
import scalajscrossproject.ScalaJSCrossPlugin.autoImport._

object InternalClient {

  def apply(id: String, base: File): CrossProject = {
    CrossProject(id, base)(JSPlatform)
      .withoutSuffixFor(JSPlatform)
      .crossType(CrossType.Full)
      .enablePlugins(Client)
      .jsSettings(
        scalaJSLinkerConfig ~= { config =>
          config
            .withModuleKind(ModuleKind.ESModule)
            .withESFeatures(_.withESVersion(ESVersion.ES2015))
        },
        libraryDependencies += "org.scala-js" %%% "scalajs-dom" % Versions.`scala-js`,
        libraryDependencies ++= Dependencies.clientWebJars
      )
      .settings(
        libraryDependencies += "org.scala-lang.modules" %%% "scala-xml" % Versions.`scala-xml`
      )
      .settings(
        organization := "glngn"
      )
  }
}
