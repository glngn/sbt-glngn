package glngn.sbt

import sbt._
// import coursier.sbtcoursier.CoursierPlugin
import sbt.Keys._
import sbtassembly.AssemblyPlugin

object NonModular extends AutoPlugin {
  override def trigger  = noTrigger
  override def requires = AssemblyPlugin

  object autoImport {
    lazy val glngnAssemblyDirectory = settingKey[Option[File]]("glngnAssemblyDirectory")
  }
  import autoImport._

  override lazy val globalSettings = Seq(
    glngnAssemblyDirectory := None,
    resolvers += Resolver.url(
      "glngn server assemblies",
      url("https://glngn-server-releases.s3.amazonaws.com/assemblies")
    )(Resolver.ivyStylePatterns)
  )
}
