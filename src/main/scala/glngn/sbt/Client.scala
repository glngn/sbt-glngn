package glngn.sbt

// import coursier.sbtcoursier.CoursierPlugin
import spray.revolver.RevolverPlugin

object Client extends Base {
  override def requires = super.requires && RevolverPlugin

  object autoImport {}

  override lazy val projectSettings = baseSettings ++ Seq()

  override lazy val buildSettings = Seq()

  override lazy val globalSettings = Seq()
}
