package glngn.sbt

// import coursier.sbtcoursier.CoursierPlugin
import sbt.Keys._

abstract class AssetServer extends Service {

  // override def requires = super.requires && SbtWeb && WebScalaJS

  lazy val assetServerSettings = serviceSettings ++ Seq(
    // scalaJSPipeline/devCommands := Seq.empty,
    // Assets / pipelineStages := Seq(scalaJSPipeline),
    libraryDependencies ++= Dependencies.assetServer,
    libraryDependencies ++= Dependencies.assetServerTest,
    // TODO: should be picked up from client?
    libraryDependencies ++= Dependencies.clientWebJars
  )

  override lazy val projectSettings = assetServerSettings
}
