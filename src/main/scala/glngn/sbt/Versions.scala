package glngn.sbt

object Versions extends SelfVersion {
  val `antlr4-runtime`       = "4.13.2"
  val aws                    = "1.12.777"
  val `case-app`             = "2.1.0-M12"
  val cats                   = "2.12.0"
  val `cats-effect`          = "3.5.5"
  val circe                  = "0.14.10"
  val `circe-generic-extras` = "0.14.4"
  val `circe-yaml`           = "1.15.0"
  val `commons-io`           = "2.17.0"
  // max prior to ZIO 2
  val distage                      = "1.1.0-M23"
  val fastparse                    = "3.1.1"
  val `json-schema-validator`      = "1.5.2"
  val `kind-projector`             = "0.13.3"
  val log4j                        = "2.24.1"
  val `log4j-api-scala`            = "13.1.0"
  val `outr-hasher`                = "1.2.3"
  val monocle                      = "3.3.0"
  val neme                         = "0.0.5"
  val pekko                        = "1.1.2"
  val `pekko-http`                 = "1.1.0"
  val `pekko-kryo-serialization`   = "1.2.0"
  val `pekko-management`           = "1.1.0"
  val `pekko-persistence-dynamodb` = "1.1.0+2-39be407d-SNAPSHOT"
  val scala                        = "2.13.15"
  val scalacheck                   = "1.18.1"

  object `scala-graph` {
    val core        = "1.13.6"
    val constrained = "1.13.2"
  }
  val `scala-collection-contrib` = "0.4.0"
  val `scala-js`                 = "1.0.0"
  val `scala-xml`                = "2.3.0"
  val scalactic                  = "3.2.19"
  val scalatestplus              = "3.2.2.0"
  val `scala-typed-holes`        = "0.1.9"
  val `scodec-bits`              = "1.2.1"
  val shapeless                  = "2.3.12"
  val sl4j                       = "2.0.16"
  val `spray-json`               = "1.3.6"
  val `sqlite-jdbc`              = "3.42.0.0"
  val sttp                       = "1.7.11"
  val swagger                    = "2.2.25"
  val `swagger-parser`           = "2.1.23"
  val `testcontainers-scala`     = "0.41.4"
  val upickle                    = "4.0.2"
  val zio                        = "1.0.18"
  val `zio-interop-cats`         = "13.0.0.2"
  val `zio-prelude`              = "1.0.0-RC6"

  // blocked by BIO

  // val zio = "2.0.5"
  // val zioInteropCats = "23.0.0.0"

  // https://github.com/zio/zio-schema/
  val `zio-schema` = "0.4.1"
}
