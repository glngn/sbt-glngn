package glngn.sbt

import NonModular.autoImport._
import sbt._
// import coursier.sbtcoursier.CoursierPlugin
import sbt.Keys._
import sbtassembly.AssemblyPlugin.autoImport._
import sbtcrossproject.CrossPlugin.autoImport._
import sbtcrossproject.CrossProject

object StarterDeployment extends Service {
  override def requires = super.requires && NonModular

  val starterDeploymentSettings = serviceSettings

  override lazy val projectSettings = starterDeploymentSettings

  def apply(
    id: String,
    base: File,
    mainClassId: String = "glngn.server.host.DefaultHostApp"
  ): CrossProject = {
    CrossProject(id, base)(JVMPlatform)
      .withoutSuffixFor(JVMPlatform)
      .crossType(CrossType.Pure)
      .enablePlugins(StarterDeployment)
      .jvmSettings(
        Test / fork := true
      )
      .settings(
        Compile / mainClass              := Some(mainClassId),
        assembly / assemblyJarName       := id + "-assembly.jar",
        assembly / mainClass             := Some(mainClassId),
        Compile / run / mainClass        := Some(mainClassId),
        Compile / packageBin / mainClass := Some(mainClassId),
        libraryDependencies ++= {
          glngnAssemblyDirectory.value match {
            case None =>
              Seq("glngn" %% "glngn-server-assembly" % Versions.self classifier "assembly")
            case Some(_) => Seq.empty
          }
        },
        Compile / unmanagedJars ++= {
          glngnAssemblyDirectory.value match {
            case None      => Seq.empty
            case Some(dir) => Seq(dir / "glngn-server-assembly.jar")
          }
        }
      )
  }
}
