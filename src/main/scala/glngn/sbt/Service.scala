package glngn.sbt

import GlngnGlobalPlugin.autoImport._
import com.typesafe.sbt.packager.archetypes.JavaAppPackaging
import fm.sbt.S3ResolverPlugin
import sbt._
import sbt.Keys._
import sbt.plugins.JvmPlugin
import sbtassembly.AssemblyPlugin.autoImport._
import sbtdynver.DynVerPlugin
import sbtwelcome.UsefulTask
import sbtwelcome.WelcomePlugin
import spray.revolver.RevolverPlugin
import spray.revolver.RevolverPlugin.autoImport._

abstract class Service extends Base {

  import com.typesafe.sbt.packager.docker._
  import com.typesafe.sbt.packager.universal.UniversalPlugin

  override def requires =
    super.requires &&
      JvmPlugin && RevolverPlugin && S3ResolverPlugin &&
      JavaAppPackaging && DockerPlugin && WelcomePlugin

  object autoImport {
    lazy val runStateDir          = settingKey[File]("runStateDir")
    lazy val runPort              = settingKey[Int]("runPort")
    lazy val containerServerPort  = settingKey[Int]("containerServerPort")
    lazy val containerOpsPort     = settingKey[Int]("containerOpsPort")
    lazy val containerClusterPort = settingKey[Int]("containerClusterPort")

    lazy val log4jConfigs = settingKey[List[String]]("""
      log4j 2 configurations (property log4j.configurationFile):
      https://logging.apache.org/log4j/2.x/manual/configuration.html#CompositeConfiguration
    """)
  }

  import DockerPlugin.autoImport._
  import DynVerPlugin.autoImport._
  import UniversalPlugin.autoImport._
  import WelcomePlugin.autoImport._
  import autoImport._

  lazy val defaultRunArgs: Def.Initialize[String] = Def.setting {
    val args = Seq(
      "--port",
      runPort.value.toString,
      "--disable-ops",
      "run",
      "--initial-node-count",
      "1",
      "--state-path",
      runStateDir.value.getAbsolutePath
    )
    s" ${args.mkString(" ")}"
  }

  lazy val defaultRun = inputKey[Unit]("runs using the runPort and runStateDir")

  lazy val serviceSettings = baseSettings ++ Seq(
    libraryDependencies ++= Dependencies.service,
    libraryDependencies ++= Dependencies.serviceTest,
    assembly / assemblyMergeStrategy := {
      // correct? These appear to be incorrectly duplicated in jackson
      case "module-info.class" => MergeStrategy.discard
      case x                   => ((assembly / assemblyMergeStrategy).value)(x)
    },
    runStateDir := {
      val out = new File(target.value, "run-state.glngn")
      IO.createDirectory(out)
      out
    },
    runPort := 10501,
    reStartArgs := {
      Seq(
        "--port",
        (Compile / run / runPort).value.toString,
        "--disable-ops",
        "run",
        "--initial-node-count",
        "1",
        "--state-dir",
        runStateDir.value.getAbsolutePath
      )
    },
    reStart := {
      val _ = runStateDir.value
      reStart.evaluated
    },
    Compile / run / fork := true,
    // magic: https://www.scala-sbt.org/1.x/docs/Howto-Dynamic-Input-Task.html
    Compile / defaultRun := Defaults
      .runTask(
        Runtime / fullClasspath,
        Compile / run / mainClass,
        Compile / run / runner
      )
      .evaluated,
    Compile / run := (Def.inputTaskDyn {
      val input = defaultRunArgs.value
      Def.taskDyn {
        (Compile / defaultRun).toTask(input)
      }
    }).evaluated,
    Test / javaOptions ++= Seq(
      "-Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager",
      "-Dlog4j2.contextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector",
      "-Dlog4j.configurationFile=log4j2-test.xml"
    ),
    reStart / javaOptions ++= Seq(
      "-Dlog4j.configurationFile=log4j2-glngn-standalone.xml",
      "-Dlog4j2.contextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector",
      "-Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager",
      "-Dfile.encoding=UTF8",
      "-XX:+UseStringDeduplication"
    ),
    Universal / javaOptions ++= Seq(
      "-Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager"
    ),
    containerServerPort  := 10501,
    containerOpsPort     := 10502,
    containerClusterPort := 10503,
    log4jConfigs         := List.empty[String],
    dockerExposedPorts := Seq(
      containerServerPort.value,
      containerOpsPort.value,
      containerClusterPort.value
    ),
    dockerCommands ++= Seq(
      ExecCmd("CMD", "--help"),
      Cmd("WORKDIR", "/var/lib/glngn"),
      ExecCmd("CMD", "chmod", "g+ws", "/var/lib/glngn")
    ),
    dockerEnvVars := Map(
      "GLNGN_CLUSTER_SERVER_PORT"          -> containerServerPort.value.toString,
      "GLNGN_CLUSTER_OPS_PORT"             -> containerOpsPort.value.toString,
      "GLNGN_CLUSTER_CLUSTER_PORT"         -> containerClusterPort.value.toString,
      "GLNGN_STANDALONE_DEFAULT_PORT"      -> containerServerPort.value.toString,
      "GLNGN_STANDALONE_DEFAULT_STATE_DIR" -> "glngn.state",
      "GLNGN_MODE"                         -> "infer"
    ),
    dockerExposedVolumes += "/var/lib/glngn",
    dockerBaseImage    := "adoptopenjdk:12-openj9-bionic",
    dockerUpdateLatest := true,
    dockerAliases := {
      val dynver = dynverInstance.value
      if (dynver.isSnapshot && dockerUpdateLatest.value) {
        Seq(dockerAlias.value.withTag(Option("latest")))
      } else {
        dockerAliases.value
      }
    },
    logo :=
      """
        | Welcome! This is a glngn server project. http://docs.glngn.com
        | """.stripMargin,
    usefulTasks ++= Seq(
      UsefulTask("c", "Test/compile", "Compile main and test modules."),
      UsefulTask(
        "d",
        "Docker/publishLocal",
        "Build a docker image and publish to current docker host."
      ),
      UsefulTask("s", "show stage", "Build and package a launcher."),
      UsefulTask("", "show runStateDir", "The state directory used by the `run` task"),
      UsefulTask("r", "run", s"Run the server on the port ${runPort.value} (setting `runPort`)")
    )
  )

  override lazy val projectSettings = serviceSettings
}
