package glngn.sbt

// import coursier.sbtcoursier.CoursierPlugin

abstract class Shared extends Base {
  object autoImport {}

  override lazy val buildSettings = Seq()

  override lazy val globalSettings = Seq()
}
