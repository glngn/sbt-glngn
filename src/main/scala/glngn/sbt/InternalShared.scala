package glngn.sbt

// import coursier.sbtcoursier.CoursierPlugin
import org.portablescala.sbtplatformdeps.PlatformDepsPlugin.autoImport._
import sbt._
import sbt.Keys._
import sbtcrossproject.CrossPlugin.autoImport._
import sbtcrossproject.CrossProject
import scalajscrossproject.ScalaJSCrossPlugin.autoImport._

object InternalShared extends Shared {

  def apply(id: String, base: File): CrossProject = {
    CrossProject(id, base)(JSPlatform, JVMPlatform)
      .crossType(CrossType.Pure)
      .enablePlugins(InternalShared)
      .jsSettings(
        libraryDependencies += "org.scala-js" %%% "scalajs-dom" % Versions.`scala-js`
      )
      .jvmSettings(
        libraryDependencies += "org.scala-js" %% "scalajs-stubs" % Versions.`scala-js` % "provided"
      )
      .settings(
        organization := "glngn"
      )
  }
}
