package glngn.sbt

import sbt._
// import coursier.sbtcoursier.CoursierPlugin
import sbt.Keys._
import sbtassembly.AssemblyPlugin.autoImport._
import sbtcrossproject.CrossPlugin.autoImport._
import sbtcrossproject.CrossProject

object InternalDeployment extends Service {

  override lazy val projectSettings = serviceSettings ++ Seq(
    organization := "glngn"
  )

  def apply(id: String, base: File, mainClassId: String): CrossProject = {
    CrossProject(id, base)(JVMPlatform)
      .withoutSuffixFor(JVMPlatform)
      .crossType(CrossType.Full)
      .enablePlugins(InternalDeployment)
      .settings(
        assembly / test                  := {},
        test / aggregate                 := false,
        assembly / assemblyJarName       := id + "-assembly.jar",
        assembly / mainClass             := Some(mainClassId),
        Compile / run / mainClass        := Some(mainClassId),
        Compile / packageBin / mainClass := Some(mainClassId)
      )
  }
}
