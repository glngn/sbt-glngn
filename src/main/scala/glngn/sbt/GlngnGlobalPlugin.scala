package glngn.sbt

import scala.util._

import sbt._
import sbt.Keys._
import sbtcrossproject.CrossProject
import sbtdynver.DynVerPlugin
import sbtwelcome.UsefulTask
import sbtwelcome.WelcomePlugin
import sys.process.Process

object GlngnGlobalPlugin extends AutoPlugin {
  override def trigger  = allRequirements
  override def requires = super.requires && DynVerPlugin && WelcomePlugin

  object autoImport {

    lazy val nextVersionTag =
      settingKey[Option[String]]("next version to tag assuming at least a patch increment")

    implicit class ScalaJSWebCrossProjectAdditions(project: CrossProject) {
      def serveToClient(other: CrossProject): CrossProject = project
      // project.settings(
      // scalaJSProjects := Seq(other.js)
      // )
    }
  }

  import WelcomePlugin.autoImport._
  import autoImport._

  lazy val tagNextVersion = Command.command("tagNextVersion") { state0 =>
    val logger = state0.log
    val ext    = Project.extract(state0)
    import ext._

    val baseDir          = ((ThisBuild / baseDirectory) get structure.data).get
    val next: String     = ((ThisBuild / nextVersionTag) get structure.data).get.get
    val buildTag: String = Try(scala.sys.env("BUILD_NUMBER")).getOrElse("SNAPSHOT")

    logger.info(s"baseDir = ${baseDir}")
    logger.info(s"next = ${next}")
    logger.info(s"buildTag = ${buildTag}")

    val tagCmd =
      Process(List("git", "tag", "-a", "-m", "tagging release build " + buildTag, next), baseDir)

    Try(tagCmd !!) match {
      case Success(out) => logger.info(out)
      case Failure(ex)  => throw ex
    }

    state0
  }

  override lazy val globalSettings = Seq(
    commands += tagNextVersion,
    ThisBuild / crossVersion := CrossVersion.binary,
    nextVersionTag := {
      import DynVerPlugin.autoImport.dynverGitDescribeOutput

      (ThisBuild / dynverGitDescribeOutput).value.flatMap { gitOutput =>
        val TagSplit = """^v(\d+)\.(\d+).(\d+)""".r
        gitOutput.ref.value match {
          case TagSplit(major, minor, patch) => {
            val nextPatch = (patch.toInt + 1).toString
            val out       = s"v${major}.${minor}.${nextPatch}"
            Some(out)
          }
          case _ => None
        }
      }
    },
    resolvers ++= Resolver.sonatypeOssRepos("releases")
  )

  override lazy val projectSettings = super.projectSettings ++ Seq(
    // TODO: Clear out default welcome message. Otherwise non-glngn server projects have a
    // big "EXAMPLE" welcome.
    logo := "",
    usefulTasks ++= Seq(
      UsefulTask("", "~ Test/compile", "Watch for source changes then compile main and test")
    )
  )
}
