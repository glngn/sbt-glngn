package glngn.sbt

import sbt._
import sbt.Keys._
import sbtassembly.AssemblyPlugin.autoImport._
import sbtcrossproject.CrossPlugin.autoImport._
import sbtcrossproject.CrossProject

object InternalAssetServer extends AssetServer {

  override lazy val projectSettings = assetServerSettings ++ Seq(
    organization := "glngn"
  )

  def apply(id: String, base: File, mainClassId: String): CrossProject =
    CrossProject(id, base)(JVMPlatform)
      .withoutSuffixFor(JVMPlatform)
      .crossType(CrossType.Full)
      .enablePlugins(InternalAssetServer)
      .settings(
        assembly / test            := {},
        test / aggregate           := false,
        assembly / assemblyJarName := id + "-assembly.jar",
        assembly / mainClass       := Some(mainClassId)
      )
}
