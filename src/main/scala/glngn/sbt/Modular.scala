package glngn.sbt

import sbt._
import sbt.Keys._

object Modular extends AutoPlugin {
  override def trigger  = noTrigger
  override def requires = GlngnGlobalPlugin

  override lazy val globalSettings = Seq(
    resolvers += Resolver.url(
      "glngn server releases",
      url("https://glngn-server-releases.s3.amazonaws.com/releases")
    )(Resolver.ivyStylePatterns)
  )
}
