package glngn.sbt

import sbt._

object Dependencies {

  val aws = Seq(
    "com.amazonaws" % "aws-java-sdk-core"     % Versions.aws,
    "com.amazonaws" % "aws-java-sdk-dynamodb" % Versions.aws
  )

  val cats = Seq(
    "org.typelevel" %% "cats-core"   % Versions.cats,
    "org.typelevel" %% "cats-effect" % Versions.`cats-effect`,
    "org.typelevel" %% "cats-free"   % Versions.cats
  )

  val zio = Seq(
    "dev.zio" %% "zio"              % Versions.zio,
    "dev.zio" %% "zio-interop-cats" % Versions.`zio-interop-cats`,
    "dev.zio" %% "zio-streams"      % Versions.zio,
    "dev.zio" %% "zio-prelude"      % Versions.`zio-prelude`
    // "dev.zio" %% "zio-schema" % Versions.zioSchema,
    // "dev.zio" %% "zio-schema-derivation" % Versions.zioSchema,
    // "dev.zio" %% "zio-schema-json" % Versions.zioSchema,
    // "dev.zio" %% "zio-schema-optics" % Versions.zioSchema
  )

  val pekkoLibraries = Seq(
    "pekko-actor",
    "pekko-actor-typed",
    "pekko-cluster",
    "pekko-cluster-typed",
    "pekko-cluster-metrics",
    "pekko-cluster-sharding",
    "pekko-cluster-sharding-typed",
    "pekko-cluster-tools",
    "pekko-discovery",
    "pekko-distributed-data",
    "pekko-persistence",
    "pekko-persistence-typed",
    "pekko-persistence-query",
    "pekko-pki",
    "pekko-remote",
    "pekko-slf4j",
    "pekko-stream",
    "pekko-stream-typed"
  ).map("org.apache.pekko" %% _)

  val pekko = pekkoLibraries.map(_ % Versions.pekko)

  val `pekko-test` = Seq(
    "pekko-testkit",
    "pekko-actor-testkit-typed",
    "pekko-persistence-tck",
    "pekko-stream-testkit"
  ).map("org.apache.pekko" %% _ % Versions.pekko % Test)

  val `pekko-persistence-dynamodb` = Seq(
    "org.apache.pekko" %% "pekko-persistence-dynamodb" % Versions.`pekko-persistence-dynamodb`
  )

  val `pekko-http` = Seq(
    "pekko-http",
    "pekko-http-core",
    "pekko-http-cors",
    "pekko-http-spray-json",
    "pekko-http-xml"
  ).map("org.apache.pekko" %% _ % Versions.`pekko-http`)

  val `pekko-http-test` = Seq(
    "pekko-http-testkit"
  ).map("org.apache.pekko" %% _ % Versions.`pekko-http` % Test)

  val `pekko-http-additions` = Seq(
    "io.spray" %% "spray-json" % Versions.`spray-json`
  )

  val `pekko-management` = Seq(
    "pekko-management",
    "pekko-management-cluster-bootstrap",
    "pekko-management-cluster-http",
    "pekko-discovery-kubernetes-api"
  ).map("org.apache.pekko" %% _ % Versions.`pekko-management`)

  val log4j = Seq(
    "org.apache.logging.log4j"  % "log4j-api"        % Versions.log4j,
    "org.apache.logging.log4j" %% "log4j-api-scala"  % Versions.`log4j-api-scala`,
    "org.apache.logging.log4j"  % "log4j-core"       % Versions.log4j,
    "org.apache.logging.log4j"  % "log4j-jcl"        % Versions.log4j,
    "org.apache.logging.log4j"  % "log4j-jul"        % Versions.log4j,
    "org.apache.logging.log4j"  % "log4j-slf4j-impl" % Versions.log4j
  )

  val `log4j-additions` = Seq(
    "com.lmax"  % "disruptor" % "4.0.0",
    "org.slf4j" % "slf4j-api" % Versions.sl4j
  )

  val monocle = Seq(
    "dev.optics" %% "monocle-core"  % Versions.monocle,
    "dev.optics" %% "monocle-macro" % Versions.monocle
  )

  val swagger = Seq(
    "io.swagger.core.v3" % "swagger-models" % Versions.swagger,
    "io.swagger.core.v3" % "swagger-core"   % Versions.swagger
  )

  // application collections

  val clientWebJars = Seq(
    "org.webjars.npm" % "webcomponents__webcomponentsjs"        % "2.2.10",
    "org.webjars.npm" % "lit-element"                           % "2.2.0",
    "org.webjars.npm" % "lit-html"                              % "1.1.0",
    "org.webjars.npm" % "loose-envify"                          % "1.4.0",
    "org.webjars.npm" % "github-com-guybedford-es-module-shims" % "0.2.7",
    "org.webjars.npm" % "js-tokens"                             % "4.0.0",
    "org.webjars.npm" % "redux"                                 % "4.0.1",
    "org.webjars.npm" % "redux-thunk"                           % "2.3.0",
    "org.webjars.npm" % "symbol-observable"                     % "1.2.0",
    "org.webjars"     % "qunit"                                 % "2.9.2" % Test
  )

  val circe = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-literal",
    "io.circe" %% "circe-numbers",
    "io.circe" %% "circe-parser",
    "io.circe" %% "circe-shapes"
  ).map(_       % Versions.circe) ++ Seq(
    "io.circe" %% "circe-generic-extras" % Versions.`circe-generic-extras`,
    "io.circe" %% "circe-yaml"           % Versions.`circe-yaml`
  )

  val core = {
    val ungrouped = Seq(
      "org.scodec"                 %% "scodec-bits"                        % Versions.`scodec-bits`,
      "com.github.alexarchambault" %% "case-app"                           % Versions.`case-app`,
      "com.github.alexarchambault" %% "case-app-util"                      % Versions.`case-app`,
      "com.github.alexarchambault" %% "case-app-annotations"               % Versions.`case-app`,
      "com.lihaoyi"                %% "fastparse"                          % Versions.fastparse,
      "com.chuusai"                %% "shapeless"                          % Versions.shapeless,
      "io.netty"                    % "netty-transport-native-unix-common" % "4.1.114.Final",
      "io.7mind.izumi"             %% "distage-core"                       % Versions.distage,
      "com.softwaremill.sttp.model" %% "core" % Versions.sttp
    )

    List(
      ungrouped,
      pekko,
      `pekko-http`,
      `pekko-http-additions`,
      aws,
      log4j,
      `log4j-additions`
    ).flatten
  }

  val coreTest = Seq(
    "org.hdrhistogram"   % "HdrHistogram"    % "2.1.8",
    "org.mockito"        % "mockito-all"     % "1.10.19",
    "org.scalactic"     %% "scalactic"       % Versions.scalactic,
    "org.scalatest"     %% "scalatest"       % Versions.scalactic,
    "org.scalatestplus" %% "scalacheck-1-14" % Versions.scalatestplus,
    // "org.scalatestplus" %% "scalacheck-1-14" % "3.1.4.0" % Test,
    "org.scalatestplus"   %% "junit-4-13"                     % Versions.scalatestplus,
    "org.scalacheck"      %% "scalacheck"                     % Versions.scalacheck,
    "io.swagger.parser.v3" % "swagger-parser"                 % Versions.`swagger-parser`,
    "io.7mind.izumi"      %% "distage-testkit-core"           % Versions.distage,
    "io.7mind.izumi"      %% "distage-testkit-scalatest"      % Versions.distage,
    "io.7mind.izumi"      %% "distage-framework"              % Versions.distage,
    "io.7mind.izumi"      %% "distage-framework-docker"       % Versions.distage,
    "com.dimafeng"        %% "testcontainers-scala-scalatest" % Versions.`testcontainers-scala`
  ).map(_ % Test) ++ `pekko-test` ++ `pekko-http-test`

  val service = {
    val ungrouped = Seq(
      "com.outr"               %% "hasher"                   % Versions.`outr-hasher`,
      "commons-io"              % "commons-io"               % Versions.`commons-io`,
      "org.scala-graph"        %% "graph-core"               % Versions.`scala-graph`.core,
      "org.scala-graph"        %% "graph-constrained"        % Versions.`scala-graph`.constrained,
      "com.lihaoyi"            %% "upickle"                  % Versions.upickle,
      "io.altoo"               %% "pekko-kryo-serialization" % Versions.`pekko-kryo-serialization`,
      "org.iq80.leveldb"        % "leveldb"                  % "0.12",
      "org.scala-lang.modules" %% "scala-xml"                % Versions.`scala-xml`,
      "org.scala-lang.modules" %% "scala-collection-contrib" % Versions.`scala-collection-contrib`,
      "org.xerial"              % "sqlite-jdbc"              % Versions.`sqlite-jdbc`
    )

    List(
      ungrouped,
      `pekko-persistence-dynamodb`,
      `pekko-management`,
      core,
      cats,
      circe,
      monocle,
      swagger,
      zio
    ).flatten
  }

  val assetServer = Seq(
    "org.jsoup"   % "jsoup"                % "1.11.3",
    "org.webjars" % "webjars-locator-core" % "0.37"
  ) ++ service

  val serviceTest = Seq(
    "org.apache.commons" % "commons-lang3"         % "3.17.0"                         % Test,
    "org.antlr"          % "antlr4-runtime"        % Versions.`antlr4-runtime`        % Test,
    "org.eclipse.jetty"  % "jetty-server"          % "8.1.12.v20130726"               % Test,
    "com.networknt"      % "json-schema-validator" % Versions.`json-schema-validator` % Test
    // "com.github.java-json-tools" % "json-schema-validator" % "2.2.14" % Test,
  ) ++ coreTest

  val assetServerTest = serviceTest
}
