package glngn.sbt

// import coursier.sbtcoursier.CoursierPlugin
import sbt._
import sbtassembly.AssemblyPlugin

abstract class Base extends AutoPlugin {
  override def trigger  = noTrigger
  override def requires = GlngnGlobalPlugin && AssemblyPlugin

  val baseSettings: Seq[Def.Setting[_]] = Seq(
    Settings.baseSettings,
    Settings.issue74Workaround,
    Settings.prelude
  ).flatten

  override lazy val projectSettings = baseSettings
}

object Base extends Base
