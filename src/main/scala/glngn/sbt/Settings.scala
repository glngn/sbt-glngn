package glngn.sbt

import sbt._
import sbt.Keys._
import sbtassembly.AssemblyPlugin.autoImport.MergeStrategy
import sbtassembly.AssemblyPlugin.autoImport.PathList
import sbtassembly.AssemblyPlugin.autoImport.assembly
import sbtassembly.AssemblyPlugin.autoImport.assemblyMergeStrategy
import scalafix.sbt.ScalafixPlugin.autoImport.scalafixScalaBinaryVersion
import scalafix.sbt.ScalafixPlugin.autoImport.scalafixSemanticdb

object Settings {

  val baseSettings: Seq[Def.Setting[_]] = Seq(
    scalaVersion := Versions.scala,
    scalacOptions ++= {
      val Some(underscoreTypeLambdas) = CrossVersion.partialVersion(scalaVersion.value) map {
        case (3, _)            => Seq("-Ykind-projector:underscores")
        case (2, 13) | (2, 12) => Seq("-Xsource:3", "-P:kind-projector:underscore-placeholders")
        case v                 => sys.error(s"unsupported Scala version $v")
      }
      underscoreTypeLambdas
    },
    // ThisBuild / scalafixScalaBinaryVersion := CrossVersion.binaryScalaVersion(scalaVersion.value),
    crossScalaVersions := Seq(Versions.scala),
    scalacOptions ++= Seq(
      "-deprecation",
      "-encoding",
      "UTF-8",
      "-explaintypes",
      "-feature",
      "-language:higherKinds",
      "-language:existentials",
      "-language:postfixOps",
      "-unchecked",
      "-Yrangepos",
      "-Vimplicits",
      // "-Vimplicits-verbose-tree",
      // "-Vtype-diffs",
      "-Xlint:adapted-args",
      "-Xlint:infer-any",
      "-Xlint:nullary-unit",
      "-Xlint:package-object-classes",
      "-Xlint:private-shadow",
      "-Xlint:poly-implicit-overload",
      "-Wunused:imports",
      "-Ywarn-value-discard",
      "-Ymacro-annotations",
      "-release:11",
      // "-opt:l:method",
      // "-Ycheck:cleanup,delambdafy",
      // "-Vprint:delambdafy,jvm",
      // "-verbose",
      "-Ybackend-parallelism",
      "8",
      "-Ycache-plugin-class-loader:last-modified",
      "-Ypatmat-exhaust-depth",
      "70"
      // "-Wmacros:both",
    ),
    Compile / doc / scalacOptions += "-groups",
    addCompilerPlugin(
      "org.typelevel" %% "kind-projector" % Versions.`kind-projector` cross CrossVersion.full
    ),
    ThisBuild / semanticdbEnabled := true,
    ThisBuild / semanticdbOptions += "-P:semanticdb:synthetics:on",
    ThisBuild / semanticdbVersion := scalafixSemanticdb.revision,
    // drop semanticdb from jars.
    Compile / packageBin / mappings ~= { (mappings: Seq[(File, String)]) =>
      mappings filterNot { case (_, subpath) =>
        subpath.startsWith("META-INF/semanticdb")
      }
    },
    assembly / assemblyMergeStrategy := {
      // reason: Huge and does not seem to be required for this use?
      case PathList("META-INF", "semanticdb", _ @_*) => MergeStrategy.discard
      // reason: multiple conflicting
      case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.first
      // reason: multiple conflicting
      case "mime.types" => MergeStrategy.last
      // reason: multiple conflicting
      case PathList("zio", "BuildInfo$.class") => MergeStrategy.discard
      // reason: multiple conflicting
      case PathList("META-INF", "versions", "9", "module-info.class") => MergeStrategy.discard
      case x => ((assembly / assemblyMergeStrategy).value)(x)
    },
    // override the jaxb-api dependency
    libraryDependencies += "jakarta.xml.bind" % "jakarta.xml.bind-api" % "2.3.3",
    excludeDependencies += ExclusionRule("javax.xml.bind"),
    resolvers += Resolver.ApacheMavenSnapshotsRepo,
    resolvers ++= Resolver.sonatypeOssRepos("snapshots"),
    libraryDependencySchemes ++= Dependencies.pekkoLibraries.map(_ % VersionScheme.Always),
    Test / testOptions += Tests.Argument(TestFrameworks.ScalaTest, "-oF")
  )

  val prelude: Seq[Def.Setting[_]] = Seq(
    libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value
  )

  // https://github.com/portable-scala/sbt-crossproject/issues/74
  val issue74Workaround =
    Seq(Compile, Test).flatMap(inConfig(_) {
      unmanagedResourceDirectories ++= {
        unmanagedSourceDirectories.value
          .map(src => (src / ".." / "resources").getCanonicalFile)
          .filterNot(unmanagedResourceDirectories.value.contains)
          .distinct
      }
    })
}
