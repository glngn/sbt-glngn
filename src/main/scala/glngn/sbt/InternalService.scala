package glngn.sbt

import sbt._
// import coursier.sbtcoursier.CoursierPlugin
import sbt.Keys._
import sbtassembly.AssemblyPlugin.autoImport._
import sbtcrossproject.CrossPlugin.autoImport._
import sbtcrossproject.CrossProject

object InternalService extends Service {

  val internalServiceSettings = serviceSettings ++ Seq(
    organization := "glngn"
  )

  override lazy val projectSettings = internalServiceSettings

  def apply(id: String, base: File, mainClassId: String): CrossProject = {
    CrossProject(id, base)(JVMPlatform)
      .withoutSuffixFor(JVMPlatform)
      .crossType(CrossType.Full)
      .enablePlugins(InternalService)
      .jvmSettings(
        Test / fork := true
      )
      .settings(
        assembly / assemblyJarName       := id + "-assembly.jar",
        Compile / mainClass              := Some(mainClassId),
        Compile / run / mainClass        := Some(mainClassId),
        Compile / packageBin / mainClass := Some(mainClassId),
        assembly / mainClass             := Some(mainClassId)
      )
  }
}
