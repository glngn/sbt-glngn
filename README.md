# sbt-glngn

glngn framework build support. See [the getting started
guide](https://gitlab.com/glngn/glngn-server-examples/blob/master/README.md).

# Usage

This plugin requires sbt 1.0.0+

```scala
resolvers += Resolver.url(
  "glngn server releases",
  url("https://glngn-server-releases.s3.amazonaws.com/releases")
)(Resolver.ivyStylePatterns)

addSbtPlugin("glngn" % "sbt-glngn" % "0.7.72")
```
